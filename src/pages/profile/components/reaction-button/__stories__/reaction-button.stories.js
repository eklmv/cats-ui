import { ReactionButton } from '../reaction-button';

export default {
  title: 'ReactionButton',
  component: ReactionButton,
};

export const Default = {
  args: {
    catInfo: {},
    type: 'like',
    reacted: true,
    loading: false,
  },
};
