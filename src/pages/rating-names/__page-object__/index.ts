import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';

export class RatingPage {
  private readonly page: Page;
  public readonly errorMessageSelector: string;
  public readonly likesTableSelector: string;
  private readonly ratingNameSelector: string;
  private readonly ratingCountSelector: string;

  constructor(page: Page) {
    this.page = page;
    this.errorMessageSelector = '//div[contains(@class,"ajs-error")]';
    this.likesTableSelector = '//table[1]';
    this.ratingNameSelector = '//td[contains(@class, "rating-names_item-name")]';
    this.ratingCountSelector = '//td[contains(@class, "rating-names_item-count")]';
  }

  async openRatingPage() {
    return await test.step('Открываю страницу "Рейтинг имён" приложения', async () => {
      await this.page.goto('/rating')
    })
  }

  private async fetchRatingList(tableSelector: string): Promise<RatingList> {
    let list: RatingList = [];
    for (const row of await this.page.locator(tableSelector).locator('//tr').all()) {
      const name = await row.locator(this.ratingNameSelector).innerText();
      const count = await row.locator(this.ratingCountSelector).innerText();
      list.push({ name: name, count: Number(count) });
    }
    return list
  }

  async fetchLikesRating(): Promise<RatingList> {
    return this.fetchRatingList(this.likesTableSelector);
  }

}

export type RatingPageFixture = TestFixture<RatingPage, { page: Page }>;
export type RatingList = { name: string, count: number }[];

export const ratingPageFixture: RatingPageFixture = async ({ page }, use) => {
  const ratingPage = new RatingPage(page);
  await use(ratingPage);
};
