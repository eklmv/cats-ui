import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture } from '../__page-object__';
import { catsRating500Error } from '../__mocks__/rating-api-responses';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({
  page,
  ratingPage,
}) => {
  await page.route(catsRating500Error.urlMatcher, async route => {
    route.fulfill(catsRating500Error.response);
  });

  await ratingPage.openRatingPage();

  await expect(page.locator(ratingPage.errorMessageSelector)).toBeVisible();
  await expect(page.locator(ratingPage.errorMessageSelector)).toHaveText(
    'Ошибка загрузки рейтинга'
  );
});

test('Рейтинг котиков отображается', async ({ page, ratingPage }) => {
  await ratingPage.openRatingPage();

  await expect(page.locator(ratingPage.likesTableSelector)).toBeVisible();

  const likesRating = await ratingPage.fetchLikesRating();
  for (let i = 0; i < likesRating.length - 1; i++) {
    expect(
      likesRating[i].count,
      likesRating
        .map(value => '\n' + value.name + ': ' + value.count)
        .toString()
    ).toBeGreaterThanOrEqual(likesRating[i + 1].count);
  }
});