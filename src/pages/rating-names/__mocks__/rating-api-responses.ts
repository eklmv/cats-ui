import { URL } from 'url';

export const catsRating500Error = {
  urlMatcher: (url: URL) => url.href.endsWith('/cats/rating'),
  response: {
    status: 500,
    contentType: 'text/plain',
    body: 'Mocked Internal Server Error',
  },
};
